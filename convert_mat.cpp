/*
 * MAT-file diagnose program
 *
 * See the MATLAB API Guide for compiling information.
 *
 * Calling syntax:
 *
 *   matdgns <matfile>
 *
 * It will diagnose the MAT-file named <matfile>.
 *
 * This program demonstrates the use of the following functions:
 *
 *  matClose
 *  matGetDir
 *  matGetNextVariable
 *  matGetNextVariableInfo
 *  matOpen
 *
 * Copyright 1984-2003 The MathWorks, Inc.
 */
#include<iostream>
#include <fstream>
#include <vector>
#include <tclDecls.h>
#include <term.h>
#include "mat.h"

#include "boost/scoped_ptr.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/rng.hpp"

using namespace std;

char *mat_name, *db_name;

void Init(int argc, char** argv)
{
	if(argc<2) {
		cout<<"Convert a set of .mat files to the leveldb/lmdb format used as input for Caffe."<<endl;
		cout<<"Usage:\n convert_mat [FLAGS] MAT_NAME DB_NAME\n"<<endl;
		exit(-1);
	}
	mat_name=argv[1];
	db_name=argv[2];
}

void CreateLMDB()
{
	// Create new DB
	scoped_ptr<db::DB> db(db::GetDB(FLAGS_backend));
	db->Open(db_name, db::NEW);
	scoped_ptr<db::Transaction> txn(db->NewTransaction());

	MATFile* pmat = matOpen(mat_name, "r");
	mxArray* pa = matGetVariable(pmat, "data");

	const mwSize *dims = mxGetDimensions(pa);
	mwSize number_of_dimensions= mxGetNumberOfDimensions(pa);
	int height=dims[0],width=dims[1],channel=dims[2],number_of_samples=dims[3];
	int size=height*width*channel;

	signed char *pr = (signed char *)mxGetData(pa);

	Datum datum;
	const int kMaxKeyLength = 256;
	int count = 0;

	for(int i=0;i<number_of_samples;i++)
	{
		std::string buffer(size, ' ');
		for(int j=0;j<size;j++)
			buffer[j]=*pr++;

		datum.set_channels(channel);
		datum.set_height(height);
		datum.set_width(width);
		datum.clear_data();
		datum.clear_float_data();

		datum.set_data(buffer);
		datum.set_label(label);
		datum.set_encoded(false);

		int length = snprintf(key_cstr, kMaxKeyLength, "%10d",i);

		string out;
		CHECK(datum.SerializeToString(&out));
		txn->Put(string(key_cstr, length), out);

		if (++count % 1000 == 0) {
			// Commit db
			txn->Commit();
			txn.reset(db->NewTransaction());
			LOG(ERROR) << "Processed " << count << " files.";
		}
	}

	if (count % 1000 != 0) {
		txn->Commit();
		LOG(ERROR) << "Processed " << count << " files.";
	}

	mxDestroyArray(pa);
	matClose(pmat);
}

int main(int argc, char** argv) {
	Init(argc, argv);
	CreateLMDB();
	return 0;
}
		