// This program converts a set of images to a lmdb/leveldb by storing them
// as Datum proto buffers.
// Usage:
//   convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME
//
// where ROOTFOLDER is the root folder that holds all the images, and LISTFILE
// should be a list of files as well as their labels, in the format as
//   subfolder1/file1.JPEG 7
//   ....

#include <algorithm>
#include <fstream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>
#include <algorithm>

#include "boost/scoped_ptr.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/rng.hpp"

using namespace caffe;  // NOLINT(build/namespaces)
using std::pair;
using boost::scoped_ptr;

DEFINE_string(backend, "lmdb",
        "The backend {lmdb, leveldb} for storing the result");

int main(int argc, char** argv) {
  ::google::InitGoogleLogging(argv[0]);

#ifndef GFLAGS_GFLAGS_H_
  namespace gflags = google;
#endif

  gflags::SetUsageMessage("Convert a set of images to the leveldb/lmdb\n"
        "format used as input for Caffe.\n"
        "Usage:\n"
        "    convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME HDF5_NAME POS_WEIGHT NEG_WEIGHT\n"
        "The ImageNet dataset for the training demo is at\n"
        "    http://www.image-net.org/download-images\n");
  gflags::ParseCommandLineFlags(&argc, &argv, true);

  if (argc < 7) {
    gflags::ShowUsageWithFlagsRestrict(argv[0], "tools/convert_bin");
    return 1;
  }

   // Create new DB
  scoped_ptr<db::DB> db(db::GetDB(FLAGS_backend));
	db->Open(argv[3], db::NEW);
	scoped_ptr<db::Transaction> txn(db->NewTransaction());
  
  std::ifstream infile(argv[2]);
  std::vector<float> weights;
  
  int patch_id=0;
  while(true)
  {
	  std::string filename;
	  int label;
	  int patchnum;
	  Datum datum;
	  std::vector<std::pair<Datum,int> > datums;
	  
	  std::string root_folder(argv[1]);


	  std::streampos size = 35*35*6;
	  int file_count=0;
	  while (infile >> filename >> patchnum >> label) {
		  std::cout<<filename<<std::endl;
		  filename=root_folder+filename;
		   std::cout<<filename<<std::endl;
		  std::fstream file(filename.c_str(), ios::in|ios::binary|ios::ate);
		  if (file.is_open())
		  {
			   for(int i=0;i<patchnum;i++)
			   {
				   std::string buffer(size, ' ');
				   file.seekg(i*size,ios::beg);
				   file.read(&buffer[0],size);
				   
				   datum.set_channels(6);
				   datum.set_height(35);
				   datum.set_width(35);
				   datum.clear_data();
				   datum.clear_float_data();

				   datum.set_data(buffer);
				   datum.set_label(label);
				   datum.set_encoded(false);
			  
				   datums.push_back(std::make_pair(datum,label));
			   }
			   file.close();
		  }	  
		  file_count++;
		  if(file_count>=1000)
			break;
	  }
	  

		// randomly shuffle data
	  LOG(INFO) << "Shuffling data";
	  shuffle(datums.begin(),datums.end());
	  
	  LOG(INFO) << "A total of " << datums.size() << " images.";

	  // Storing to db
	  int count = 0;
	  const int kMaxKeyLength = 256;
	  char key_cstr[kMaxKeyLength];

	  std::ofstream myfile("labels.txt");
	  // float* weights=new float[datums.size()];
	  
	  for(int i=0;i<datums.size();i++)
	  {
		  datum=datums[i].first;
		  
		  int length = snprintf(key_cstr, kMaxKeyLength, "%08d", patch_id);
		 
		  string out;
		  CHECK(datum.SerializeToString(&out));
		 
		  txn->Put(string(key_cstr, length), out);
		  patch_id++;

		  int label=datums[i].second;
		  if(label==1)
		  {
			  weights.push_back(atof(argv[5]));
			  // weights[i]=atof(argv[5]);
		  } else {
			  weights.push_back(atof(argv[6]));
			  // weights[i]=atof(argv[6]);
		  }
		  
		  if (++count % 10000 == 0) {
			txn->Commit();
			txn.reset(db->NewTransaction());
			LOG(ERROR) << "Processed " << count << " files.";
		  }
	  }
	  if (count % 10000 != 0) {
			txn->Commit();
			if(file_count>=1000)
				txn.reset(db->NewTransaction());
			LOG(ERROR) << "Processed " << count << " files.";
	  }
	  if(file_count<1000)
			break;
  }

  hid_t file_id = H5Fcreate(argv[4], H5F_ACC_TRUNC, H5P_DEFAULT,
                       H5P_DEFAULT);

  hsize_t dims[1];
  //dims[0]=datums.size();
  dims[0]=weights.size();
  float* weightsf=new float[weights.size()];
  for(int i=0;i<weights.size();i++)
	  weightsf[i]=weights[i];

  H5LTmake_dataset_float(
      file_id, "sample_weight", 1, dims, weightsf);

  delete[] weightsf;
  
						 
  return 0;
}
// This program converts a set of images to a lmdb/leveldb by storing them
// as Datum proto buffers.
// Usage:
//   convert_imageset [FLAGS] ROOTFOLDER/ LISTFILE DB_NAME
//
// where ROOTFOLDER is the root folder that holds all the images, and LISTFILE
// should be a list of files as well as their labels, in the format as
//   subfolder1/file1.JPEG 7
//   ....

#include <algorithm>
#include <fstream>  // NOLINT(readability/streams)
#include <string>
#include <utility>
#include <vector>

#include "boost/scoped_ptr.hpp"
#include "gflags/gflags.h"
#include "glog/logging.h"

#include "caffe/proto/caffe.pb.h"
#include "caffe/util/db.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/rng.hpp"

using namespace caffe;  // NOLINT(build/namespaces)
using std::pair;
using boost::scoped_ptr;



